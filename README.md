![Example of Monthly Organizer](extras/tabs_location.png)

# Table of Contents

[[_TOC_]]

# Synopsis

Monthly Organizer is a Qt6 program written in Python made for Linux and Windows which organizes files from a source to a sink into a chosen frequency (such as monthly, bimonthly, quarterly, etc.).
The files can be compressed or uncompressed, excluded or included via regular expressions, and multiple profiles can be saved for different settings.

# Installation and Removal

For instructions on how to install or remove the Monthly Organizer, see [this page](https://gitlab.com/KodyVB/MonthlyOrganizer/-/tree/main/install).

# Usage

After selecting the desired source and destination directories and rules, pressing "Organize source" will move the desired files from the source directory to the destination directory with the chosen organization and compression options.
Changing the rules for organization and compression methods may result in completely different file structures in the destination directory, i.e. Jan.zip, Jan-Mar.tar, and Jan-Dec.
In order to consolidate or reorganize the destination directory, or filter out files according to new inclusion/exclusion rules, the "Reorganize destination" button was added.
Any files that were filtered out through the reorganization of the destination directory are placed in the "organized_dir" directory at the top level, alongside the directories labeling the years.

## Location Tab

![Location tab](extras/tabs_location.png)

The location tab is where you set the source directory, where the files you wish to organize are, and the destination directory, where the files will be organized in.
The destination directory will be parent to directories labeled by the files' years and possibly "organized_dir", so it won't automatically create a directory to hold them all.

The locations can be typed or found via the corresponding "Search" buttons.
The locations won't be used unless the relevant "Save" buttons are pressed, with the current locations shown at the bottom of the screen above the "Organize source" and "Reorganize destination" buttons.

The location text boxes have color-coded text, with the colors meaning:

- White/black (depending on theme) - it is a valid directory which matches the currently selected directory
- Green - it is a valid directory but doesn't match the currently selected directory
- Red - it is an invalid directory

At the bottom of the tab, there are also two check buttons: "Recurse through source" and "Follow symbolic links".
The former check button being checked will result in directories under the source directory having their files be organized, as well.
Similarly, the latter being checked will cause the files in symbolic links under the source directory to be organized.

## Additional Tab

![Additional tab](extras/tabs_additional.png)

The "Frequency" menu allows the user to select the following options and results:

- Monthly: Jan, Feb, Mar...
- Bimonthly: Jan-Feb, Mar-Apr, May-Jun...
- Quarterly: Jan-Mar, Apr-Jun, Jul-Sep...
- Triannually: Jan-Apr, May-Aug, Sep-Dec
- Semiannually: Jan-Jun, Jul-Dec
- Annually: Jan-Dec

The "Compression" menu allows the user to select whether the month-level frequency directories are compressed or not, in tar or zip files, and which compression method.

The "Time format" menu allows the user to select whether to organize the files by the modified, created, or accessed time (accessed time being omitted from Windows since it's not available on that OS).

The "Theme" menu allows the user to select whether to use the OS/desktop environment's default theme or dark/light themes provided by PyQtDarkTheme.

The "Organization confirmation" being checked will result in a confirmation dialog box to appear before the source directory is organized.

The "Clean source of empty directories" not being checked will result in any empty sub-directories in the source directory not being deleted at the end of the organization process.

## Included and Excluded Tabs

![Include tab](extras/tabs_included.png)

![Exclude tab](extras/tabs_excluded.png)

The list of options in these tabs use [Python's regular expressions](https://docs.python.org/3/library/re.html) to decide which files to include and exclude in the organization and reorganization processes.
By default, these only apply to file names, but the "Filter directory names" check box allows directory names to be taken into account, as well.
The exclusion rules are applied after the inclusion rules, so the default inclusion regular expression of ".*" includes all files, but the exclusions filter through those.

## Profiles Tab

![Profiles tab](extras/tabs_profiles.png)

Profiles store all of the other tabs' data in their own files.
Profiles can be created, removed, edited (the "Edit" button edits the profile name), and loaded.
If all available profiles are deleted, a new "Default" profile will be created and loaded.


# Credits

The icons were provided for free by [Freepik - Flaticon](https://www.flaticon.com/free-icons/administration).

The additional themes were provided by [5yutan5's PyQtDarkTheme](https://github.com/5yutan5/PyQtDarkTheme).

