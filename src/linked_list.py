# This file is part of Monthly Organizer.
#
# Monthly Organizer is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Monthly Organizer is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Monthly Organizer. If not, see <https://www.gnu.org/licenses/>.

class LinkedListNode:
    __slots__ = 'value', 'next_node', 'year', 'month', 'dir'

    def __init__(self, month: list, dir: list, value='', year=None):
        self.next_node: LinkedListNode | None = None
        self.value = value
        self.year = year
        self.month = month
        self.dir = dir


class LinkedList:
    __slots__ = 'head', 'tail'

    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    def __iter__(self):
        node = self.head
        while node is not None:
            yield node
            node = node.next_node

    def insert(self, value='', year=None, month=None, dir=None):
        if month is None:
            month = []

        if dir is None:
            dir = []

        node = LinkedListNode(value=value, year=year, month=month, dir=dir)
        if self.head is None:
            self.head = node
            return

        if self.tail is None:
            self.head.next_node = node
            self.tail = node
            return

        self.tail.next_node = node
        self.tail = node
