# This file is part of Monthly Organizer.
#
# Monthly Organizer is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Monthly Organizer is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Monthly Organizer. If not, see <https://www.gnu.org/licenses/>.

from PyQt6 import QtWidgets as qtw
from PyQt6 import QtCore as qtc
from PyQt6 import QtGui as qgui
from pathlib import Path
import organizers
from UiFiles import confirmation_ui
from UiFiles import input_ui
from UiFiles import main_ui
import sys
import qdarktheme
import globals
import platform


class MainApp(qtw.QApplication):
    organize_source = qtc.pyqtSignal()
    confirmation_text = qtc.pyqtSignal(list)
    confirmation_style = qtc.pyqtSignal(str)
    switch_widget_main = qtc.pyqtSignal()
    switch_widget_progress = qtc.pyqtSignal()
    input_theme_update = qtc.pyqtSignal(str)
    input_tin_update = qtc.pyqtSignal(str)

    def __init__(self, argv):
        super().__init__(argv)

        qgui.QGuiApplication.setDesktopFileName("MonthlyOrganizer")

        self.mutex = qtc.QMutex()
        self.wait_condition = qtc.QWaitCondition()
        self.confirmation_window = ConfirmationWindow()
        self.input_window = InputWindow()

        self.worker = Worker(self.mutex, self.wait_condition)
        self.worker_thread = qtc.QThread()
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.start()

        self.main_window = MainWindow()
        self.main_window.show()
        self.main_window.main_closed.connect(self.worker_thread.quit)

        # Input window general
        self.input_tin_update.connect(self.input_window.update_tin)
        self.input_theme_update.connect(self.input_window.update_theme)

        # Add to lists: include, exclude, profile
        self.main_window.but_included_add.clicked.connect(
            self.input_window.type_include
        )
        self.main_window.but_included_add.clicked.connect(
            self.input_window.action_append
        )
        self.main_window.but_included_add.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_included_add.clicked.connect(
            self.input_window.exec
        )
        self.input_window.append_include.connect(
            self.main_window.append_lst_include
        )

        self.main_window.but_excluded_add.clicked.connect(
            self.input_window.type_exclude
        )
        self.main_window.but_excluded_add.clicked.connect(
            self.input_window.action_append
        )
        self.main_window.but_excluded_add.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_excluded_add.clicked.connect(
            self.input_window.exec
        )
        self.input_window.append_exclude.connect(
            self.main_window.append_lst_exclude
        )

        self.main_window.but_profile_add.clicked.connect(
            self.input_window.type_profile
        )
        self.main_window.but_profile_add.clicked.connect(
            self.input_window.action_append
        )
        self.main_window.but_profile_add.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_profile_add.clicked.connect(
            self.input_window.exec
        )
        self.input_window.append_profile.connect(
            self.main_window.append_lst_profile
        )

        # Remove from lists: include, exclude, profile
        self.main_window.but_profile_remove.clicked.connect(
            self.main_window.remove_lst_profile
        )
        self.main_window.but_included_remove.clicked.connect(
            self.main_window.remove_lst_include
        )
        self.main_window.but_excluded_remove.clicked.connect(
            self.main_window.remove_lst_exclude
        )

        # Edit list items: include, exclude, profile
        self.main_window.but_included_edit.clicked.connect(
            self.input_window.type_include
        )
        self.main_window.but_included_edit.clicked.connect(
            self.input_window.action_edit
        )
        self.main_window.but_included_edit.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_included_edit.clicked.connect(
            self.edit_lst_include
        )
        self.input_window.edit_include.connect(
            self.main_window.change_lst_include
        )

        self.main_window.but_excluded_edit.clicked.connect(
            self.input_window.type_exclude
        )
        self.main_window.but_excluded_edit.clicked.connect(
            self.input_window.action_edit
        )
        self.main_window.but_excluded_edit.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_excluded_edit.clicked.connect(
            self.edit_lst_exclude
        )
        self.input_window.edit_exclude.connect(
            self.main_window.change_lst_exclude
        )

        self.main_window.but_profile_edit.clicked.connect(
            self.input_window.type_profile
        )
        self.main_window.but_profile_edit.clicked.connect(
            self.input_window.action_edit
        )
        self.main_window.but_profile_edit.clicked.connect(
            self.input_window_theme_updater
        )
        self.main_window.but_profile_edit.clicked.connect(
            self.edit_lst_profile
        )
        self.input_window.edit_profile.connect(
            self.main_window.change_lst_profile
        )

        # Change to existing profile
        self.main_window.but_profile_load.clicked.connect(
            self.main_window.load_lst_profile
        )

        # Check box toggles
        self.main_window.chk_sym.clicked.connect(self.toggle_chk_symbolic)
        self.main_window.chk_recurse.clicked.connect(self.toggle_chk_recurse)
        self.main_window.chk_confirmation.clicked.connect(
            self.toggle_chk_confirmation
        )
        self.main_window.chk_source_clean.clicked.connect(
            self.toggle_chk_source_clean
        )
        self.main_window.chk_include_dirs.clicked.connect(
            self.toggle_chk_include_dirs
        )
        self.main_window.chk_exclude_dirs.clicked.connect(
            self.toggle_chk_exclude_dirs
        )

        # Selection list updaters
        self.main_window.sel_time.currentIndexChanged.connect(
            self.main_window.change_sel_time
        )
        self.main_window.sel_frequency.currentIndexChanged.connect(
            self.main_window.change_sel_frequency
        )
        self.main_window.sel_compression.currentIndexChanged.connect(
            self.main_window.change_sel_compression
        )
        self.main_window.sel_theme.currentIndexChanged.connect(
            self.main_window.change_sel_theme
        )

        # Directory save buttons
        self.main_window.but_sink_save.clicked.connect(
            self.main_window.save_sink
        )
        self.main_window.but_source_save.clicked.connect(
            self.main_window.save_source
        )

        # Directory browser buttons
        self.main_window.but_sink_search.clicked.connect(
            self.main_window.browse_sink
        )
        self.main_window.but_source_search.clicked.connect(
            self.main_window.browse_source
        )

        # Organize and reorganize buttons
        self.main_window.but_organize.clicked.connect(
            self.switch_widget_progress
        )
        self.main_window.but_organize.clicked.connect(self.worker.organize)
        self.main_window.but_reorganize.clicked.connect(
            self.switch_widget_progress
        )
        self.main_window.but_reorganize.clicked.connect(self.worker.reorganize)

        # Confirmation window
        self.confirmation_window.but_box_confirmation.accepted.connect(
            self.confirmation_accept
        )
        self.confirmation_window.but_box_confirmation.rejected.connect(
            self.confirmation_reject
        )
        self.confirmation_window.confirm_closed.connect(
            self.confirmation_reject
        )
        self.worker.continue_job.connect(self.wait_condition.wakeAll)
        self.worker.confirmation_list.connect(self.update_confirmation)
        self.confirmation_style.connect(self.confirmation_window.update_theme)
        self.confirmation_text.connect(self.confirmation_window.update_list)

        # Progress window
        self.switch_widget_main.connect(self.main_window.switch_widget_main)
        self.switch_widget_progress.connect(
            self.main_window.switch_widget_progress
        )

        self.worker.progress_update.connect(self.main_window.update_progress)
        self.worker.job_done.connect(self.main_window.but_progress_done_enable)
        self.main_window.but_progress_done.clicked.connect(
            self.switch_widget_main
        )
        self.main_window.but_progress_done.clicked.connect(
            self.main_window.but_progress_done_disable
        )
        self.main_window.but_progress_done.clicked.connect(
            self.worker.confirmation_reset
        )
        self.main_window.but_progress_done.clicked.connect(
            self.main_window.txt_progress.clear
        )

    @qtc.pyqtSlot()
    def input_window_theme_updater(self):
        self.input_theme_update.emit(get_theme(globals.data.theme))

    @qtc.pyqtSlot()
    def confirmation_accept(self):
        self.worker.confirmation_accepted()

    @qtc.pyqtSlot()
    def confirmation_reject(self):
        self.worker.confirmation_rejected()

    @qtc.pyqtSlot(list)
    def update_confirmation(self, files_to_change):
        """Updates the theme and text for the confirmation box"""
        self.confirmation_style.emit(get_theme(globals.data.theme))
        self.confirmation_text.emit(files_to_change)
        self.confirmation_window.show()

    @qtc.pyqtSlot()
    def toggle_chk_symbolic(self):
        """Toggles the symbolic link boolean"""
        globals.data.follow_sym = self.main_window.chk_sym.isChecked()
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def toggle_chk_recurse(self):
        """Toggles the recurse boolean"""
        globals.data.recurse = self.main_window.chk_recurse.isChecked()
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def toggle_chk_confirmation(self):
        """Toggles the move confirmation boolean"""
        globals.data.move_confirmation = (
            self.main_window.chk_confirmation.isChecked()
        )
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def toggle_chk_source_clean(self):
        """Toggles the source cleaning boolean"""
        globals.data.clean_source = (
            self.main_window.chk_source_clean.isChecked()
        )
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def toggle_chk_include_dirs(self):
        """Toggles the boolean to include regex filtering in directories"""
        globals.data.search_include_dirs = (
            self.main_window.chk_include_dirs.isChecked()
        )
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def toggle_chk_exclude_dirs(self):
        """Toggles the boolean to exclude regex filtering in directories"""
        globals.data.search_exclude_dirs = (
            self.main_window.chk_exclude_dirs.isChecked()
        )
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def edit_lst_include(self):
        """
        Edits the name of the selected item on the include list via a text
        input window
        """
        for selected_item in self.main_window.lst_included.selectedItems():
            self.input_tin_update.emit(selected_item.text())
            self.input_window.exec()

    @qtc.pyqtSlot()
    def edit_lst_exclude(self):
        """
        Edits the name of the selected item on the exclude list via a text
        input window
        """
        for selected_item in self.main_window.lst_excluded.selectedItems():
            self.input_tin_update.emit(selected_item.text())
            self.input_window.exec()

    @qtc.pyqtSlot()
    def edit_lst_profile(self):
        """
        Edits the name of the selected item on the profile list via a text
        input window
        """
        for selected_item in self.main_window.lst_profiles.selectedItems():
            self.input_tin_update.emit(selected_item.text())
            self.input_window.exec()


class MainWindow(qtw.QMainWindow, main_ui.Ui_MainWindow):
    main_closed = qtc.pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.update_window_options()
        self.sel_time_os_clean()

        # Text input updaters
        self.tin_sink.textChanged.connect(self.check_tin_sink)
        self.tin_source.textChanged.connect(self.check_tin_source)
        self.tin_sink.installEventFilter(self)
        self.tin_source.installEventFilter(self)

    def sel_time_os_clean(self):
        """Accessed time isn't available for Windows, so remove the option"""
        if platform.system() == 'Windows':
            self.sel_time.removeItem(0)

    @qtc.pyqtSlot(str, str)
    def change_lst_profile(self, new_name, old_name):
        """Function passed to the text input window in edit_lst_profile"""
        if new_name == '' or new_name in globals.data.profile_list:
            return

        globals.data.change_profile_name(old_name=old_name, new_name=new_name)
        self.update_window_options()

    @qtc.pyqtSlot(str, str)
    def change_lst_include(self, new_name, old_name):
        """Changes the selected item to the new name if valid for include"""
        index = globals.data.search_include.index(old_name)

        if new_name == '':
            globals.data.search_include.pop(index)

        elif new_name in globals.data.search_include:
            return

        else:
            globals.data.search_include[index] = new_name

        globals.data.write_profile_data()
        self.update_lsts()

    @qtc.pyqtSlot(str, str)
    def change_lst_exclude(self, new_name, old_name):
        """Changes the selected item to the new name if valid for exclude"""
        index = globals.data.search_exclude.index(old_name)

        if new_name == '':
            globals.data.search_exclude.pop(index)

        elif new_name in globals.data.search_exclude:
            return

        else:
            globals.data.search_exclude[index] = new_name

        globals.data.write_profile_data()
        self.update_lsts()

    @qtc.pyqtSlot(str)
    def append_lst_include(self, new_item):
        """Adds the new item to the include list in data if valid"""
        if new_item != '' and new_item not in globals.data.search_include:
            globals.data.search_include.append(new_item)
            globals.data.write_profile_data()
            self.update_lsts()

    @qtc.pyqtSlot(str)
    def append_lst_exclude(self, new_item):
        """Adds the new item to the exclude list in data if valid"""
        if new_item != '' and new_item not in globals.data.search_exclude:
            globals.data.search_exclude.append(new_item)
            globals.data.write_profile_data()
            self.update_lsts()

    @qtc.pyqtSlot(str)
    def append_lst_profile(self, new_item):
        """Adds the new item to the profile list in data if valid"""
        if new_item != '' and new_item not in globals.data.profile_list:
            globals.data.profile_name = new_item
            self.update_window_options()

    @qtc.pyqtSlot()
    def remove_lst_exclude(self):
        """
        Removes a selected option from the excluded regex list, updating
        profile data too
        """
        for selected_item in self.lst_excluded.selectedItems():
            index = globals.data.search_exclude.index(selected_item.text())
            globals.data.search_exclude.pop(index)

        globals.data.write_profile_data()
        self.update_lsts()

    @qtc.pyqtSlot()
    def remove_lst_include(self):
        """
        Removes a selected option from the included regex list, updating
        profile data too
        """
        for selected_item in self.lst_included.selectedItems():
            index = globals.data.search_include.index(selected_item.text())
            globals.data.search_include.pop(index)

        globals.data.write_profile_data()
        self.update_lsts()

    @qtc.pyqtSlot()
    def remove_lst_profile(self):
        """
        Removes a selected option from the profile list, updating profile data
        too
        """
        for selected_profile in self.lst_profiles.selectedItems():
            globals.data.profile_pop(selected_profile.text())

        globals.data.write_profile_data()
        self.update_window_options()

    @qtc.pyqtSlot()
    def load_lst_profile(self):
        """Switches profiles to the selected one"""
        for selected_profile in self.lst_profiles.selectedItems():
            if selected_profile.text() != globals.data.profile_name:
                globals.data.profile_name = selected_profile.text()
                globals.data.write_profile_data()
                self.update_window_options()

    @qtc.pyqtSlot()
    def change_sel_compression(self):
        """Changes the compression option"""
        globals.data.compression = self.sel_compression.currentText()
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def change_sel_frequency(self):
        """Changes the organization frequency option"""
        globals.data.frequency = self.sel_frequency.currentText()
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def change_sel_time(self):
        """Changes the time format option"""
        globals.data.file_time_format = self.sel_time.currentText()
        globals.data.write_profile_data()

    @qtc.pyqtSlot()
    def save_source(self):
        """Saves the source directory to what's in tin"""
        source_path = Path(self.tin_source.toPlainText())
        if not source_path.is_dir():
            self.check_tins()
            return

        if str(source_path) == globals.data.source:
            return

        globals.data.source = str(source_path)
        globals.data.write_profile_data()
        self.update_window_options(tins=False)
        self.update_tins(sink=False)

    @qtc.pyqtSlot()
    def save_sink(self):
        """Saves the sink directory to what's in tin"""
        sink_path = Path(self.tin_sink.toPlainText())
        if not sink_path.is_dir():
            self.check_tins()
            return

        if str(sink_path) == globals.data.sink:
            return

        globals.data.sink = str(sink_path)
        globals.data.write_profile_data()
        self.update_window_options(tins=False)
        self.update_tins(source=False)

    @qtc.pyqtSlot()
    def browse_sink(self):
        """Opens file browser to set sink tin"""
        sink = qtw.QFileDialog.getExistingDirectory(
            self, 'Sink file', directory=str(globals.data.sink)
        )

        if not sink:
            return

        self.tin_sink.clear()
        self.tin_sink.insertPlainText(sink)

    @qtc.pyqtSlot()
    def browse_source(self):
        """Opens file browser to set source tin"""
        source = qtw.QFileDialog.getExistingDirectory(
            self, 'Source file', directory=str(globals.data.source)
        )

        if not source:
            return

        self.tin_source.clear()
        self.tin_source.insertPlainText(source)

    @qtc.pyqtSlot(str)
    def update_progress(self, txt):
        """Adds to progress text box, scrolling as appended"""
        horScrollBar = self.txt_progress.horizontalScrollBar()
        verScrollBar = self.txt_progress.verticalScrollBar()
        scrollIsAtEnd = verScrollBar.maximum() - verScrollBar.value() <= 10

        self.txt_progress.append(txt)

        if scrollIsAtEnd:
            verScrollBar.setValue(verScrollBar.maximum())
            horScrollBar.setValue(0)

    @qtc.pyqtSlot()
    def switch_widget_progress(self):
        """Switches the main window to the progress display"""
        self.stackedWidget.setCurrentWidget(self.stack_progress)

    @qtc.pyqtSlot()
    def switch_widget_main(self):
        """Switches the main window to the main display"""
        self.stackedWidget.setCurrentWidget(self.stack_main)

    @qtc.pyqtSlot()
    def but_progress_done_enable(self):
        """Enables the progress done button to go back to main screen"""
        self.but_progress_done.setEnabled(True)

    @qtc.pyqtSlot()
    def but_progress_done_disable(self):
        """
        Disables the progress done button so the user can't run two instances
        """
        self.but_progress_done.setEnabled(False)

    def eventFilter(self, obj, event):
        """Disables enter/return key for GUI"""
        if event.type() == qtc.QEvent.Type.KeyPress:
            if event.key() in (qtc.Qt.Key.Key_Enter, qtc.Qt.Key.Key_Return):
                return True
        return super().eventFilter(obj, event)

    def update_but_organizers(self):
        """Checks both organizer buttons"""
        self.check_but_organize()
        self.check_but_reorganize()

    def check_but_organize(self):
        """
        Checks if the current source is valid to make organize button valid
        """
        sink_path = Path(self.txt_sink.text())
        source_path = Path(self.txt_source.text())
        if sink_path.is_dir() and source_path.is_dir():
            self.but_organize.setEnabled(True)
        else:
            self.but_organize.setEnabled(False)

    def check_but_reorganize(self):
        """
        Checks if the current sink is valid to make reorganize button valid
        """
        sink_path = Path(self.txt_sink.text())
        if sink_path.is_dir():
            self.but_reorganize.setEnabled(True)
        else:
            self.but_reorganize.setEnabled(False)

    def check_tins(self):
        """Checks both tins"""
        self.check_tin_sink()
        self.check_tin_source()

    def check_tin_sink(self):
        """Checks the validity of the sink in the input box"""
        current_tin_path = Path(self.tin_sink.toPlainText())
        tin_is_dir = current_tin_path.is_dir()
        if str(current_tin_path) == globals.data.sink and tin_is_dir:
            self.tin_sink.setStyleSheet('')
            self.but_sink_save.setEnabled(True)
        elif tin_is_dir:
            self.tin_sink.setStyleSheet('color: green')
            self.but_sink_save.setEnabled(True)
        else:
            self.tin_sink.setStyleSheet('color: red')
            self.but_sink_save.setEnabled(False)

    def check_tin_source(self):
        """Checks the validity of the source in the input box"""
        current_tin_path = Path(self.tin_source.toPlainText())
        tin_is_dir = current_tin_path.is_dir()
        if str(current_tin_path) == globals.data.source and tin_is_dir:
            self.tin_source.setStyleSheet('')
            self.but_source_save.setEnabled(True)
        elif tin_is_dir:
            self.tin_source.setStyleSheet('color: green')
            self.but_source_save.setEnabled(True)
        else:
            self.tin_source.setStyleSheet('color: red')
            self.but_source_save.setEnabled(False)

    def update_theme(self):
        """Updates the theme to the selected one"""
        self.style_sheet = get_theme(globals.data.theme)
        self.setStyleSheet(self.style_sheet)
        if self.style_sheet == qdarktheme.load_stylesheet('light'):
            text = 'Light'
        elif self.style_sheet == qdarktheme.load_stylesheet('dark'):
            text = 'Dark'
        else:
            text = 'Default'

        self.sel_theme.setCurrentText(text)

    def change_sel_theme(self):
        """Changes the theme option"""
        globals.data.theme = self.sel_theme.currentText()
        globals.data.write_profile_data()
        get_theme(globals.data.theme)
        self.style_sheet = self.style_sheet
        self.update_window_options()

    def update_window_options(self, tins: bool = True):
        """
        Updates all check boxes, labels, lists, text input boxes, and
        selections
        """
        self.update_theme()
        self.update_chks()
        self.update_txts()
        self.update_sels()
        self.update_lsts()
        if tins:
            self.update_tins()
        self.update_but_organizers()

    def update_chks(self):
        """Updates all check boxes"""
        self.chk_sym.setChecked(globals.data.follow_sym)
        self.chk_recurse.setChecked(globals.data.recurse)
        self.chk_source_clean.setChecked(globals.data.clean_source)
        self.chk_confirmation.setChecked(globals.data.move_confirmation)
        self.chk_include_dirs.setChecked(globals.data.search_include_dirs)
        self.chk_exclude_dirs.setChecked(globals.data.search_exclude_dirs)

    def update_lsts(self):
        """Updates all lists"""
        globals.data.read_profile_list()
        self.lst_profiles.clear()
        for count, item in enumerate(globals.data.profile_list):
            self.lst_profiles.insertItem(count, item)

        self.lst_included.clear()
        for count, item in enumerate(globals.data.search_include):
            self.lst_included.insertItem(count, item)

        self.lst_excluded.clear()
        for count, item in enumerate(globals.data.search_exclude):
            self.lst_excluded.insertItem(count, item)

    def update_sels(self):
        """Updates all selection boxes"""
        self.sel_time.setCurrentText(globals.data.file_time_format)
        self.sel_frequency.setCurrentText(globals.data.frequency)
        self.sel_compression.setCurrentText(globals.data.compression)

    def update_txts(self):
        """Updates all labels"""
        self.txt_sink.setText(str(globals.data.sink))
        if Path(self.txt_sink.text()).is_dir():
            self.txt_sink.setStyleSheet('')
        else:
            self.txt_sink.setStyleSheet('color: red')

        self.txt_source.setText(str(globals.data.source))
        if Path(self.txt_source.text()).is_dir():
            self.txt_source.setStyleSheet('')
        else:
            self.txt_source.setStyleSheet('color: red')

        self.txt_profile_current.setText(globals.data.profile_name)

    def update_tins(self, sink: bool = True, source: bool = True):
        """Updates all text input boxes"""
        if sink:
            self.tin_sink.clear()
            self.tin_sink.insertPlainText(str(globals.data.sink))
        if source:
            self.tin_source.clear()
            self.tin_source.insertPlainText(str(globals.data.source))
        self.check_tins()

    def closeEvent(self, event):
        self.main_closed.emit()
        event.accept()


class InputWindow(qtw.QDialog, input_ui.Ui_Dialog):
    append_include = qtc.pyqtSignal(str)
    append_exclude = qtc.pyqtSignal(str)
    append_profile = qtc.pyqtSignal(str)
    edit_include = qtc.pyqtSignal(str, str)
    edit_exclude = qtc.pyqtSignal(str, str)
    edit_profile = qtc.pyqtSignal(str, str)

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.tin_name.installEventFilter(self)
        self.but_name_confirm.clicked.connect(self.message_send)
        self.but_name_cancel.clicked.connect(self.close)
        self.list_type = 'None'
        self.action_type = 'None'
        self.default_txt = ''

    @qtc.pyqtSlot()
    def action_append(self):
        self.action_type = 'append'

    @qtc.pyqtSlot()
    def action_edit(self):
        self.action_type = 'edit'

    @qtc.pyqtSlot()
    def type_include(self):
        self.list_type = 'include'

    @qtc.pyqtSlot()
    def type_exclude(self):
        self.list_type = 'exclude'

    @qtc.pyqtSlot()
    def type_profile(self):
        self.list_type = 'profile'

    @qtc.pyqtSlot(str)
    def update_theme(self, theme):
        self.setStyleSheet(theme)

    @qtc.pyqtSlot(str)
    def update_tin(self, default_txt):
        self.tin_name.clear()
        self.tin_name.insertPlainText(default_txt)
        self.default_txt = default_txt

    def eventFilter(self, obj, event):
        if event.type() == qtc.QEvent.Type.KeyPress:
            if event.key() in (qtc.Qt.Key.Key_Enter, qtc.Qt.Key.Key_Return):
                return True
        return super().eventFilter(obj, event)

    @qtc.pyqtSlot()
    def message_send(self):
        """Sends the text to the main application"""
        if self.action_type == 'append':
            if self.list_type == 'include':
                self.append_include.emit(self.tin_name.toPlainText())
            elif self.list_type == 'exclude':
                self.append_exclude.emit(self.tin_name.toPlainText())
            elif self.list_type == 'profile':
                self.append_profile.emit(self.tin_name.toPlainText())

        elif self.action_type == 'edit':
            if self.list_type == 'include':
                self.edit_include.emit(
                    self.tin_name.toPlainText(), self.default_txt
                )
            elif self.list_type == 'exclude':
                self.edit_exclude.emit(
                    self.tin_name.toPlainText(), self.default_txt
                )
            elif self.list_type == 'profile':
                self.edit_profile.emit(
                    self.tin_name.toPlainText(), self.default_txt
                )

        self.close()

    def closeEvent(self, event):
        self.list_type = 'None'
        self.action_type = 'None'
        self.default_txt = ''
        self.tin_name.clear()
        event.accept()


class ConfirmationWindow(qtw.QDialog, confirmation_ui.Ui_Dialog):
    confirm_closed = qtc.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setupUi(self)

    @qtc.pyqtSlot(str)
    def update_theme(self, theme):
        self.setStyleSheet(theme)

    @qtc.pyqtSlot(list)
    def update_list(self, things: list):
        self.txt_confirmation_browser.setText('')
        for each_thing in things:
            self.txt_confirmation_browser.insertPlainText(
                f'{str(each_thing)}\n'
            )

    def closeEvent(self, event):
        self.confirm_closed.emit()
        event.accept()


class Worker(qtc.QObject):
    progress_update = qtc.pyqtSignal(str)
    confirmation_list = qtc.pyqtSignal(list)
    job_done = qtc.pyqtSignal()
    continue_job = qtc.pyqtSignal()

    def __init__(self, mutex, wait_condition):
        self.mutex = mutex
        self.wait_condition = wait_condition
        super().__init__()
        self.confirmation_got = 0

    @qtc.pyqtSlot()
    def confirmation_reset(self):
        self.confirmation_got = 0

    @qtc.pyqtSlot()
    def confirmation_accepted(self):
        self.confirmation_got = 1
        self.continue_job.emit()

    @qtc.pyqtSlot()
    def confirmation_rejected(self):
        self.confirmation_got = -1
        self.continue_job.emit()

    @qtc.pyqtSlot()
    def organize(self):
        data_organize = organizers.data_for_organization(worker=self)
        data_organize.source = globals.data.source
        data_organize.sink = globals.data.sink
        data_organize.dir_names = globals.data.dir_names
        data_organize.compression = globals.data.compression
        data_organize.include = globals.data.search_include
        data_organize.exclude = globals.data.search_exclude
        data_organize.filter_includes = globals.data.search_include_dirs
        data_organize.filter_excludes = globals.data.search_exclude_dirs
        data_organize.time_format = globals.data.file_time_format
        data_organize.recurse_option = globals.data.recurse
        data_organize.follow_sym = globals.data.follow_sym
        data_organize.clean_source = globals.data.clean_source
        data_organize.confirmation_required = globals.data.move_confirmation
        data_organize.wait_condition = self.wait_condition
        data_organize.mutex = self.mutex

        organizers.organize_source(data_organization=data_organize)
        self.job_done.emit()

    @qtc.pyqtSlot()
    def reorganize(self):
        data_reorganize = organizers.data_for_organization(worker=self)
        data_reorganize.sink = globals.data.sink
        data_reorganize.dir_names = globals.data.dir_names
        data_reorganize.compression = globals.data.compression
        data_reorganize.include = globals.data.search_include
        data_reorganize.exclude = globals.data.search_exclude
        data_reorganize.filter_includes = globals.data.search_include_dirs
        data_reorganize.filter_excludes = globals.data.search_exclude_dirs
        data_reorganize.time_format = globals.data.file_time_format

        organizers.reorganize_sink(data_reorganization=data_reorganize)
        self.job_done.emit()


def get_theme(theme):
    """Gets the theme from the selection to be used on all windows"""
    if theme == 'Dark':
        style_sheet = qdarktheme.load_stylesheet('dark')
    elif theme == 'Light':
        style_sheet = qdarktheme.load_stylesheet('light')
    else:
        style_sheet = ''

    return style_sheet


def main():
    app = MainApp(sys.argv)
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
