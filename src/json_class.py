# This file is part of Monthly Organizer.
#
# Monthly Organizer is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Monthly Organizer is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Monthly Organizer. If not, see <https://www.gnu.org/licenses/>.

from os import getenv as os_getenv
from json import load as json_load
from json import dump as json_dump
from pathlib import Path
import platform
import globals


class CacheData:
    """
    Reads, creates, and changes the following data in json:
        Frequency, source, sink, compression type, whether search is recursive,
        whether to follow symbolic links, which file time to check, and profile
        name for all profiles.
    """

    def __init__(self):
        """
        Initializes and retrieves: profile_dir, current_profile, profile_name,
        frequency, dir_names, source, sink, recurse, compression, follow_sym,
        profile_dir
        """

        self.month_list = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ]

        self.freq_translator = {
            'Monthly': 1,
            'Bimonthly': 2,
            'Quarterly': 3,
            'Triannually': 4,
            'Semiannually': 6,
            'Annually': 12,
        }

        self.find_profile_dir()
        self.init_dir(self.profile_dir)
        self.find_cache_dir()
        self.init_dir(self.cache_dir)
        self.current_profile = self.cache_dir.joinpath('current_profile')
        self.read_profile_name()
        self.update_profile_selection()

    def profile_pop(self, option: str) -> None:
        """
        Removes an option from the profile list if it's there, and updates
        profile_file if necessary, making new Default profile if none others
        exist
        """
        if option not in self.profile_list:
            return

        self.profile_list.pop(self.profile_list.index(option))
        option_path = Path(self.profile_dir).joinpath(option).absolute()
        if option_path.is_file():
            option_path.unlink()

        if self.profile_name == option:
            if len(self.profile_list) > 0:
                self.profile_name = self.profile_list[0]
            else:
                self.profile_name = 'Default'

    def update_profile_selection(self) -> None:
        """
        Updates the profile file variable and reads the profile data,
        creating new profile if there is none under profile name.
        """
        self.profile_file = self.profile_dir.joinpath(self.profile_name)
        if not self.profile_file.is_file():
            self.create_profile()
        self.read_profile_data()

    def find_cache_dir(self) -> None:
        """
        Cache dirs change based on OS depending on cache homes, so this finds
        it and sets cache_dir.
        """
        if platform.system() == 'Linux':
            cache_base = os_getenv('XDG_CACHE_HOME')
            if cache_base is None:
                cache_base = str(os_getenv('HOME'))
                self.cache_dir = Path(cache_base).joinpath(
                    '.cache', globals.PROGRAM_NAME
                )
            else:
                self.cache_dir = Path(cache_base).joinpath(
                    globals.PROGRAM_NAME
                )

        elif platform.system() == 'Windows':
            cache_base = str(os_getenv('LOCALAPPDATA'))
            self.cache_dir = Path(cache_base).joinpath(
                globals.PROGRAM_NAME, 'cache'
            )

        elif platform.system() == 'Darwin':
            cache_base = str(os_getenv('HOME'))
            self.cache_dir = Path(cache_base).joinpath(
                'Library', 'Preferences', globals.PROGRAM_NAME, 'cache'
            )

    def find_profile_dir(self) -> None:
        """
        Profile dirs change based on OS depending on config homes, so this
        finds it and sets profile_dir.
        """
        if platform.system() == 'Linux':
            var_base = os_getenv('XDG_CONFIG_HOME')
            if var_base is None:
                var_base = str(os_getenv('HOME'))
                self.profile_dir = Path(var_base).joinpath(
                    '.config', globals.PROGRAM_NAME
                )
            else:
                self.profile_dir = Path(var_base).joinpath(
                    globals.PROGRAM_NAME
                )

        elif platform.system() == 'Windows':
            var_base = str(os_getenv('LOCALAPPDATA'))
            self.profile_dir = Path(var_base).joinpath(
                globals.PROGRAM_NAME, 'config'
            )

        elif platform.system() == 'Darwin':
            var_base = str(os_getenv('HOME'))
            self.profile_dir = Path(var_base).joinpath(
                'Library', 'Preferences', globals.PROGRAM_NAME, 'config'
            )

    def get_home(self) -> str:
        """
        Returns the $HOME or equivalent directory of the OS as Path object
        """
        if platform.system() == 'Linux':
            return str(os_getenv('HOME'))

        elif platform.system() == 'Windows':
            base = str(os_getenv('USERPROFILE'))
            desktop = str(Path(base).joinpath('Desktop'))
            return desktop

        else: # platform.system() == 'Darwin':
            return str(os_getenv('HOME'))

    def init_dir(self, dir) -> None:
        """If directory not there, make it"""
        if not dir.is_dir():
            dir.mkdir(parents=True)

    def read_profile_list(self) -> None:
        """Gets list of profiles from profile directory"""
        self.profile_list = []
        for each in self.profile_dir.iterdir():
            if each.suffix == '' and each.is_file():
                self.profile_list.append(each.stem)

    def create_profile(self) -> None:
        """
        Creates the following data in json:
        Frequency, source, sink, compression type, whether search is recursive,
        whether to follow symbolic links, inclusion/exclusion regex, mca file
        time formats, and whether to require confirmation before organizing
        """
        home_dir = self.get_home()
        json_data = {
            'dir_names': [
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
                'Jan-Dec',
            ],
            'frequency': 'Annually',
            'sink': home_dir,
            'source': home_dir,
            'recurse': False,
            'compression': 'Directories',
            'follow_sym': False,
            'search_include': ['.*'],
            'search_include_dirs': False,
            'search_exclude': globals.COMPRESSED_SUFFIXES,
            'search_exclude_dirs': False,
            'file_time_format': globals.TIME_MODIFIED,
            'move_confirmation': True,
            'clean_source': True,
            'theme': 'Default',
        }

        with open(self.profile_file, 'w') as f:
            json_dump(json_data, f, sort_keys=True, indent=4)

    def read_profile_name(self) -> None:
        """
        Gets the currently used profile name from the profile,
        creates it if not there. Sets current_profile and profile_name
        """
        if self.current_profile.is_file():
            with open(self.current_profile, 'r') as f:
                self.profile_name = f.read().splitlines()[0]
        else:
            self.profile_name = 'Default'
            self.write_profile_name()

    def read_profile_data(self) -> None:
        """
        Gets the following data from the json:
        Compression, frequency, dir_names, source, sink, recurse, follow_sym
        """
        with open(self.profile_file, 'r') as f:
            saved_data = json_load(f)

        self.compression = saved_data['compression']
        self.frequency = saved_data['frequency']
        self.dir_names = saved_data['dir_names']
        self.source = saved_data['source']
        self.sink = saved_data['sink']
        self.recurse = saved_data['recurse']
        self.follow_sym = saved_data['follow_sym']
        self.search_include = saved_data['search_include']
        self.search_include_dirs = saved_data['search_include_dirs']
        self.search_exclude = saved_data['search_exclude']
        self.search_exclude_dirs = saved_data['search_exclude_dirs']
        self.file_time_format = saved_data['file_time_format']
        self.move_confirmation = saved_data['move_confirmation']
        self.clean_source = saved_data['clean_source']
        self.theme = saved_data['theme']

    def write_profile_name(self) -> None:
        """Writes profile name to the current_profile"""
        with open(self.current_profile, 'w') as f:
            f.write(self.profile_name)

    def change_profile_name(self, old_name: str, new_name: str) -> None:
        """Changes the name of a profile from old_name to new_name"""
        if old_name not in self.profile_list:
            return

        if new_name in self.profile_list:
            self.profile_pop(new_name)

        parent = Path(self.profile_dir)
        parent.joinpath(old_name).rename(parent.joinpath(new_name))
        if self.profile_name == old_name:
            self.profile_name = new_name

    def write_profile_data(self) -> None:
        """
        Sets the following data in the json:
        Compression, frequency, dir_names, source, sink, recurse, follow_sym
        """
        saved_data = {}

        saved_data['compression'] = self.compression
        saved_data['frequency'] = self.frequency
        saved_data['dir_names'] = self.dir_names
        saved_data['source'] = self.source
        saved_data['sink'] = self.sink
        saved_data['recurse'] = self.recurse
        saved_data['follow_sym'] = self.follow_sym
        saved_data['search_include'] = self.search_include
        saved_data['search_include_dirs'] = self.search_include_dirs
        saved_data['search_exclude'] = self.search_exclude
        saved_data['search_exclude_dirs'] = self.search_exclude_dirs
        saved_data['file_time_format'] = self.file_time_format
        saved_data['move_confirmation'] = self.move_confirmation
        saved_data['clean_source'] = self.clean_source
        saved_data['theme'] = self.theme

        with open(self.profile_file, 'w') as f:
            json_dump(saved_data, f, indent=4)

    @property
    def profile_name(self) -> str:
        """Returns the current profile"""
        return self._profile_name

    @profile_name.setter
    def profile_name(self, profile: str):
        """
        Changes profile by writing to current_profile and reading from profile.
        """
        self._profile_name = profile
        self.write_profile_name()
        self.update_profile_selection()

    @property
    def source(self) -> str:
        """Returns source directory"""
        if isinstance(self._source, Path):
            return str(self._source.absolute())

        return str(Path(self._source).absolute())

    @source.setter
    def source(self, source: str):
        """Sets source directory"""
        if isinstance(source, Path):
            self._source = str(source.absolute())
        else:
            self._source = str(Path(source).absolute())

    @property
    def sink(self) -> str:
        """Returns sink directory"""
        if isinstance(self._sink, Path):
            return str(self._sink.absolute())

        return str(Path(self._sink).absolute())

    @sink.setter
    def sink(self, sink: str):
        """Sets sink directory"""
        if isinstance(sink, Path):
            self._sink = str(sink.absolute())
        else:
            self._sink = str(Path(sink).absolute())

    @property
    def frequency(self) -> str:
        """Returns the frequency."""
        return self._frequency

    @frequency.setter
    def frequency(self, frequency: str):
        """
        Sets the frequency and the month structure based on frequency.
        Expects: Monthly, Bimonthly, Quarterly, Triannually, Semiannually,
        or Annually
        """
        self._frequency = frequency
        freq_num = self.freq_translator[self._frequency]

        if freq_num == 1:
            self.dir_names = self.month_list.copy()
            return

        self.dir_names = []
        for i in range(0, len(self.month_list), freq_num):
            for _ in range(freq_num):
                self.dir_names.append(
                    f'{self.month_list[i]}-'
                    f'{self.month_list[i+(freq_num-1)]}'
                )
