# This file is part of Monthly Organizer.
#
# Monthly Organizer is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Monthly Organizer is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Monthly Organizer. If not, see <https://www.gnu.org/licenses/>.

from dataclasses import dataclass, field
from PyQt6 import QtCore as qtc
from linked_list import LinkedList
from main_gui import Worker
from os import utime as os_utime
from datetime import datetime
from pathlib import Path
import globals
import tarfile
import zipfile
import time
import re


@dataclass
class data_for_organization:
    """
    All of the data required to organize the source or reorganize the sink
    """

    # Data not commented "organize only" is needed for both organizing the
    # source and reorganizing the sink
    worker: Worker
    source: str = field(default_factory=str) # organize only
    sink: str = field(default_factory=str)
    dir_names: list = field(default_factory=list)
    compression: str = field(default_factory=str)
    include: list = field(default_factory=list)
    exclude: list = field(default_factory=list)
    filter_includes: bool = field(default_factory=bool)
    filter_excludes: bool = field(default_factory=bool)
    time_format: str = field(default_factory=str)
    recurse_option: bool = field(default_factory=bool)  # organize only
    follow_sym: bool = field(default_factory=bool)  # organize only
    clean_source: bool = field(default_factory=bool)  # organize only
    mutex: qtc.QMutex | None = None  # organize only
    wait_condition: qtc.QWaitCondition | None = None  # organize only
    confirmation_required: bool = field(default_factory=bool)  # organize only


def reorganize_sink(data_reorganization: data_for_organization) -> None:
    """
    Sends all files to a temp_dir directory to consolidate them all into a
    single format, in case there's multiple zip/tar/dir formats already.
    Also sends excluded globs to organizedDir
    """
    sink_path = to_path(data_reorganization.sink)
    if not any(sink_path.iterdir()):
        return

    data_reorganization.sink = str(sink_path)

    # Move all files to the temporary directory
    temp_dir = sink_path.joinpath('temp_dir')
    temp_dir_str = str(temp_dir)
    if not temp_dir.exists():
        temp_dir.mkdir()

    for each_file in sink_path.iterdir():
        if each_file != temp_dir:
            data_reorganization.worker.progress_update.emit(
                f'Moving file: "{str(each_file)}"...'
            )
            safe_move(each_file, temp_dir)

    # Find all compressed files in temporary directory
    dir_ll = get_sub_directories(
        data_reorganization.sink,
        follow_sym=False,
        recurse_option=True,
        worker=data_reorganization.worker,
        include_globs=data_reorganization.include,
        exclude_globs=data_reorganization.exclude,
        filter_includes=False,
        filter_excludes=False,
    )
    file_ll = LinkedList()

    for each_dir in dir_ll:
        add_files_to_ll(
            source_dir=each_dir.value,
            current_list=file_ll,
            include_globs=globals.COMPRESSED_SUFFIXES,
            exclude_globs=[],
            time_format=data_reorganization.time_format,
            dir_names=data_reorganization.dir_names,
            worker=data_reorganization.worker,
        )

    # Safely extract all files in the temporary directory
    extract_ll(file_ll=file_ll, worker=data_reorganization.worker)

    # Do organized_source() on temporary directory
    data_reorganization.source = temp_dir_str
    data_reorganization.recurse_option = True
    data_reorganization.follow_sym = False
    data_reorganization.clean_source = False
    data_reorganization.mutex = None
    data_reorganization.wait_condition = None
    organize_source(data_reorganization)

    # Move all remaining files from temporary directory to organized_dir
    dir_ll = get_sub_directories(
        temp_dir_str,
        follow_sym=False,
        recurse_option=True,
        worker=data_reorganization.worker,
        include_globs=data_reorganization.include,
        exclude_globs=data_reorganization.exclude,
        filter_includes=False,
        filter_excludes=False,
    )
    file_ll = LinkedList()

    for each_dir in dir_ll:
        add_files_to_ll(
            source_dir=each_dir.value,
            current_list=file_ll,
            include_globs=['.*'],
            exclude_globs=[],
            time_format=data_reorganization.time_format,
            dir_names=data_reorganization.dir_names,
            worker=data_reorganization.worker,
        )

    organized_dir = sink_path.joinpath('organized_dir')
    if not organized_dir.is_dir():
        organized_dir.mkdir()

    for each_file in file_ll:
        data_reorganization.worker.progress_update.emit(
            f'Moving file: "{each_file.value}"...'
        )
        safe_move(source_file=each_file.value, sink_dir=organized_dir)

    # Delete empty temporary directory
    remove_directories(
        temp_dir, follow_sym=False, worker=data_reorganization.worker
    )
    remove_directories(
        organized_dir, follow_sym=False, worker=data_reorganization.worker
    )


def remove_directories(
    directory: Path,
    follow_sym: bool,
    worker: Worker,
    protect_source: bool = False,
) -> None:
    """
    Recursively deletes any empty directories in a given directory
    """
    for each_file in directory.iterdir():
        if each_file.is_dir():
            if each_file.is_symlink() and not follow_sym:
                pass
            else:
                remove_directories(each_file, follow_sym, worker=worker)

    if not any(directory.iterdir()) and not protect_source:
        if directory.is_symlink() and follow_sym:
            worker.progress_update.emit(
                f'Removing sym link: "{str(directory)}"...'
            )
            directory.unlink()
        elif not directory.is_symlink():
            worker.progress_update.emit(
                f'Removing directory: "{str(directory)}"...'
            )
            directory.rmdir()


def extract_ll(file_ll: LinkedList, worker: Worker) -> None:
    """
    Extracts all zip and tar files in a given linked list
    """
    if any(file_ll):
        for each_file in file_ll:
            parent = Path(each_file.value).parent
            if each_file.value[-4:] == '.zip':
                stem = Path(each_file.value).stem
                zip = True
                tar = False

            else:
                stem = Path(each_file.value)
                for _ in stem.suffixes:
                    stem = Path(stem.stem)
                zip = False
                tar = True

            directory_name = Path(parent).joinpath(stem)
            prepare_sink_dir(
                directory=directory_name,
                compressed_file=each_file.value,
                worker=worker,
                tar=tar,
                zip=zip,
            )


def organize_source(data_organization: data_for_organization) -> None:
    """
    Collects the source's files' information then sends them to the target
    and compresses accordingly
    """
    source_path = to_path(data_organization.source)
    data_organization.sink = str(to_path(data_organization.sink))
    data_organization.source = str(source_path)

    dir_ll = get_sub_directories(
        data_organization.source,
        follow_sym=data_organization.follow_sym,
        recurse_option=data_organization.recurse_option,
        worker=data_organization.worker,
        include_globs=data_organization.include,
        exclude_globs=data_organization.exclude,
        filter_includes=data_organization.filter_includes,
        filter_excludes=data_organization.filter_excludes,
    )
    file_ll = LinkedList()

    for each_dir in dir_ll:
        add_files_to_ll(
            source_dir=each_dir.value,
            current_list=file_ll,
            include_globs=data_organization.include,
            exclude_globs=data_organization.exclude,
            time_format=data_organization.time_format,
            dir_names=data_organization.dir_names,
            worker=data_organization.worker,
        )

    if data_organization.confirmation_required:
        things = []
        for each_file in file_ll:
            things.append(each_file.value)

        data_organization.worker.confirmation_list.emit(things)
        if (
            data_organization.mutex is not None
            and data_organization.wait_condition is not None
        ):
            data_organization.mutex.lock()
            data_organization.wait_condition.wait(data_organization.mutex)
            data_organization.mutex.unlock()

        if data_organization.worker.confirmation_got == -1:
            return

    if not any(file_ll):
        if data_organization.clean_source:
            remove_directories(
                directory=source_path,
                follow_sym=data_organization.follow_sym,
                worker=data_organization.worker,
                protect_source=True,
            )
        return

    unique_date_ll = get_unique_date_combos(file_ll)

    if data_organization.compression == 'Directories':
        data_organization.worker.progress_update.emit(
            'Making required organizational directories...'
        )
        make_required_ll_dirs(
            target_dir=data_organization.sink,
            unique_dates=unique_date_ll,
            worker=data_organization.worker,
        )
        move_ll_to_dirs(
            target_dir=data_organization.sink,
            file_list=file_ll,
            worker=data_organization.worker,
        )
    else:
        prepare_dates_ll(
            unique_date_ll=unique_date_ll,
            target=data_organization.sink,
            compression=data_organization.compression,
            worker=data_organization.worker,
        )
        make_required_ll_dirs(
            target_dir=data_organization.sink,
            unique_dates=unique_date_ll,
            worker=data_organization.worker,
        )
        move_ll_to_dirs(
            target_dir=data_organization.sink,
            file_list=file_ll,
            worker=data_organization.worker,
        )
        compress_dates_ll(
            unique_date_ll=unique_date_ll,
            target=data_organization.sink,
            compression=data_organization.compression,
            worker=data_organization.worker,
        )

    if data_organization.clean_source:
        remove_directories(
            directory=source_path,
            follow_sym=data_organization.follow_sym,
            worker=data_organization.worker,
            protect_source=True,
        )


def compress_dates_ll(
    unique_date_ll: LinkedList, target: str, compression: str, worker: Worker
) -> None:
    """
    Compresses all of the directories in the linked list using tar_directory
    or zip_directory
    """
    for each_yr in unique_date_ll:
        for each_dir in set(each_yr.dir):
            destination = Path(target).joinpath(str(each_yr.year), each_dir)
            worker.progress_update.emit(
                f'Compressing directory: "{str(destination)}"...'
            )
            if compression[:3] == 'zip':
                zip_directory(source_dir=destination, compression=compression)
            elif compression[:3] == 'tar':
                tar_directory(source_dir=destination, compression=compression)


def prepare_dates_ll(
    unique_date_ll: LinkedList, target: str, compression: str, worker: Worker
) -> None:
    """
    Prepares the sink directories in the unique dates linked list for the files
    to be moved before compression
    """
    for each_yr in unique_date_ll:
        for each_dir in set(each_yr.dir):
            destination = Path(target).joinpath(str(each_yr.year), each_dir)
            compressed_file, _ = get_compressed_name(destination, compression)
            if compression[:3] == 'zip':
                prepare_sink_dir(
                    destination, compressed_file, worker=worker, zip=True
                )
            elif compression[:3] == 'tar':
                prepare_sink_dir(
                    destination, compressed_file, worker=worker, tar=True
                )


def prepare_sink_dir(
    directory,
    compressed_file,
    worker: Worker,
    tar: bool = False,
    zip: bool = False,
) -> None:
    """
    If the compressed file is already there, this decompresses it, if the
    uncompressed directory is also there, it moves the files to an existing
    temp directory or renames the uncompressed directory to the temp directory,
    then extracts the compressed file, removes the compressed file, and then
    moves any temp directory files to the uncompressed directory, deleting
    any empty temp directory
    """
    directory = to_path(directory)
    compressed_file = to_path(compressed_file)

    temp_dir = directory.parent.joinpath(f'temp{directory.name}')

    if compressed_file.exists():
        if directory.exists():
            if temp_dir.exists():
                for each_file in directory.iterdir():
                    worker.progress_update.emit(
                        f'Moving file: "{str(each_file)}"...'
                    )
                    safe_move(each_file, temp_dir)
            else:
                directory.rename(temp_dir)

        worker.progress_update.emit(
            f'Extracting directory: "{str(compressed_file)}"...'
        )
        extract_file(compressed_file, directory, zip=zip, tar=tar)

    if temp_dir.exists():
        for each_file in temp_dir.iterdir():
            worker.progress_update.emit(f'Moving file: "{str(each_file)}"...')
            safe_move(each_file, directory)
        if not any(temp_dir.iterdir()):
            temp_dir.rmdir()


def get_compressed_name(directory, compression) -> tuple[str, str]:
    """
    Returns the zip/tar file name and required tar string or zipfile type for
    compression
    """
    if 'tar' in compression:
        if 'BZIP2' in compression:
            selected_mode = 'w:bz2'
            compressed_file = f'{directory}.tar.bz2'
        elif 'GZIP' in compression:
            selected_mode = 'w:gz'
            compressed_file = f'{directory}.tar.gz'
        elif 'LZMA' in compression:
            selected_mode = 'w:xz'
            compressed_file = f'{directory}.tar.lzma'
        else:
            selected_mode = 'w'
            compressed_file = f'{directory}.tar'

    # elif 'zip' in compression:
    else:
        compressed_file = f'{directory}.zip'

        if 'BZIP2' in compression:
            selected_mode = str(zipfile.ZIP_BZIP2)
        elif 'Deflated' in compression:
            selected_mode = str(zipfile.ZIP_DEFLATED)
        elif 'LZMA' in compression:
            selected_mode = str(zipfile.ZIP_LZMA)
        else:
            selected_mode = str(zipfile.ZIP_STORED)

    return compressed_file, selected_mode


def tar_directory(source_dir, compression: str) -> None:
    """
    Sends all files in given directory (Path or str) to tar file in the same
    parent directory, compressing if given the type
    """
    source_dir = to_path(source_dir)

    tar_file, selected_mode = get_compressed_name(source_dir, compression)

    with tarfile.open(tar_file, mode=selected_mode) as tfile:
        for each_file in source_dir.iterdir():
            tfile.add(each_file, each_file.name)
            each_file.unlink()

    if not any(source_dir.iterdir()):
        source_dir.rmdir()


def zip_directory(source_dir, compression: str) -> None:
    """
    Sends all files in given directory (Path or str) to zip file in the same
    parent directory, compressing if given the type
    """
    source_dir = to_path(source_dir)

    zip_file, selected_mode = get_compressed_name(source_dir, compression)

    with zipfile.ZipFile(zip_file, 'w', int(selected_mode)) as zfile:
        for each_file in source_dir.iterdir():
            zfile.write(each_file, each_file.name)
            each_file.unlink()

    if not any(source_dir.iterdir()):
        source_dir.rmdir()


def extract_file(compressed_name, destination, tar=False, zip=False) -> None:
    """
    Wrapper to automatically unzip() or untar() file
    """
    if zip:
        unzip(zip_file=compressed_name, destination=destination)
    elif tar:
        untar(tar_file=compressed_name, destination=destination)


def untar(tar_file, destination) -> None:
    """
    Extracts given tar file (Path or str) to the destination directory (Path or
    str)
    """
    with tarfile.open(tar_file, 'r') as tfile:
        tfile.extractall(destination)

    tar_file.unlink()


def unzip(zip_file, destination) -> None:
    """
    Unzips given zip file (Path or str) to the destination directory (Path or
    str) while maintaining the date/time info
    """
    destination = to_path(destination)

    with zipfile.ZipFile(zip_file, 'r') as zfile:
        for zfile_info in zfile.infolist():
            zfile.extract(zfile_info, path=destination)
            date_time = time.mktime(zfile_info.date_time + (0, 0, -1))
            os_utime(
                destination.joinpath(zfile_info.filename),
                (date_time, date_time),
            )

    zip_file.unlink()


def move_ll_to_dirs(
    target_dir: str, file_list: LinkedList, worker: Worker
) -> None:
    """
    Safely moves all of the files in the linked list to their respective
    directories, given the main sink directory
    """
    for each_file in file_list:
        complete_dir = Path(target_dir).joinpath(
            str(each_file.year), str(each_file.dir)
        )
        worker.progress_update.emit(f'Moving file: "{each_file.value}"...')
        safe_move(each_file.value, complete_dir)


def make_required_ll_dirs(
    target_dir: str, unique_dates: LinkedList, worker: Worker
) -> None:
    """
    Uses the unique dates linked list to create any years and month directories
    in the target directory
    """
    for each_year in unique_dates:
        year_path = Path(target_dir).joinpath(str(each_year.year))
        if not year_path.is_dir():
            worker.progress_update.emit(
                f'Making directory: "{str(year_path)}"...'
            )
            year_path.mkdir()

        for _, each_dir in zip(each_year.month, each_year.dir):
            complete_dir = year_path.joinpath(each_dir)
            if not complete_dir.is_dir():
                worker.progress_update.emit(
                    f'Making directory: "{str(complete_dir)}"...'
                )
                complete_dir.mkdir()


def num_to_dir(number: int, dir_names: list) -> str:
    """
    Translates the month's number to the month abbreviation
    """
    return dir_names[number - 1]


def get_unique_date_combos(file_list: LinkedList) -> LinkedList:
    """
    Initializes and returns linked list of unique year+month combinations
    and directory name (eg. Jan-Mar) for sink directory operations
    """
    unique_dates = LinkedList()
    for each_file in file_list:
        add_date_info(each_file, unique_dates)

    return unique_dates


def add_date_info(file_node, unique_dates: LinkedList) -> None:
    """
    Checks if a month/year combination exists in linked list. If the year
    doesn't exist, creates new node with year and month list. If year exists
    and month doesn't, appends month to year's node along with the dir name
    (eg. Jan-Mar). Otherwise, does nothing
    """
    for each_date in unique_dates:
        if file_node.year == each_date.year:
            if file_node.month not in each_date.month:
                each_date.month.append(file_node.month)
                each_date.dir.append(file_node.dir)
            return

    unique_dates.insert(
        value='',
        year=file_node.year,
        month=[file_node.month],
        dir=[file_node.dir],
    )


def add_files_to_ll(
    source_dir: str,
    current_list: LinkedList,
    include_globs: list,
    exclude_globs: list,
    time_format: str,
    dir_names: list,
    worker: Worker,
) -> None:
    """
    Adds files that follow given inclusion and exlcusion rules to a given
    linked list
    """
    for each_file in Path(source_dir).iterdir():
        if is_valid_file(each_file, include_globs, exclude_globs):
            year, month = get_file_time_data(each_file, time_format)
            dir = num_to_dir(month, dir_names)
            worker.progress_update.emit(f'Found file: "{str(each_file)}"...')
            current_list.insert(
                value=str(each_file), year=year, month=month, dir=dir
            )


def get_file_time_data(file_name: Path, time_format: str) -> tuple[int, int]:
    """
    Returns the year and month creation, modified, or accessed time of a given
    path object
    """
    if time_format == globals.TIME_ACCESSED:
        ctime = file_name.stat().st_atime
    elif time_format == globals.TIME_CREATED:
        ctime = file_name.stat().st_ctime
    # elif time_format == globals.TIME_MODIFIED:
    else:
        ctime = file_name.stat().st_mtime

    year = int(datetime.fromtimestamp(ctime).strftime('%Y'))
    month = int(datetime.fromtimestamp(ctime).strftime('%m'))
    return year, month


def is_valid_file(
    file_name: Path, include_globs: list, exclude_globs: list
) -> bool:
    """
    Checks if a file: is a file, has at least one include glob, and has none
    of the exclude globs
    """
    if not file_name.is_file():
        return False

    has_include = False
    for each_glob in include_globs:
        if re.search(each_glob, file_name.name):
            has_include = True

    if not has_include:
        return False

    for each_glob in exclude_globs:
        if re.search(each_glob, file_name.name):
            return False

    return True


def get_sub_directories(
    source_dir: str,
    follow_sym: bool,
    recurse_option: bool,
    worker: Worker,
    include_globs: list,
    exclude_globs: list,
    filter_includes: bool,
    filter_excludes: bool,
) -> LinkedList:
    """
    Returns a linked list of all subdirectories of the source directory;
    the subdirectories being strings
    """
    linked_list = LinkedList()
    linked_list.insert(source_dir)

    if not recurse_option:
        return linked_list

    return recurse_list_dir(
        source_dir,
        linked_list,
        follow_sym,
        worker,
        include_globs,
        exclude_globs,
        filter_includes,
        filter_excludes,
    )


def recurse_list_dir(
    source_dir: str,
    current_list: LinkedList,
    follow_sym: bool,
    worker: Worker,
    include_globs: list,
    exclude_globs: list,
    filter_includes: bool,
    filter_excludes: bool,
) -> LinkedList:
    """
    Recurses through given directory to return linked list of strings of
    directories
    """
    for each_result in Path(source_dir).iterdir():
        if each_result.is_dir():
            directory_acceptable = True
            if not follow_sym and each_result.is_symlink():
                directory_acceptable = False

            if directory_acceptable and filter_includes:
                directory_acceptable = False
                for each_glob in include_globs:
                    if re.search(each_glob, each_result.name):
                        directory_acceptable = True

            if directory_acceptable and filter_excludes:
                for each_glob in exclude_globs:
                    if re.search(each_glob, each_result.name):
                        directory_acceptable = False

            if directory_acceptable:
                worker.progress_update.emit(
                    f'Found directory: "{str(each_result.absolute())}"...'
                )
                current_list.insert(str(each_result.absolute()))
                recurse_list_dir(
                    source_dir=str(each_result.absolute()),
                    current_list=current_list,
                    follow_sym=follow_sym,
                    worker=worker,
                    include_globs=include_globs,
                    exclude_globs=exclude_globs,
                    filter_includes=filter_includes,
                    filter_excludes=filter_excludes,
                )

    return current_list


def safe_move(source_file, sink_dir) -> None:
    """
    Moves a source file to a destination, but if the destination already has
    the source file's name, it addes an incremental counter to the source file
    until the name is unique to the destination
    """
    source_file = to_path(source_file)
    sink_dir = to_path(sink_dir)

    file_name = source_file.name

    if not source_file.exists() or not sink_dir.is_dir():
        return

    # Just move the file if the name is unique and end function
    if not sink_dir.joinpath(file_name).exists():
        source_file.rename(sink_dir.joinpath(file_name))
        return

    # Otherwise, add a " (1)" to the file name if not already there and
    # increment until it's unique, otherwise start over at (1)
    i = 1
    if re.search(' [(][0-9]+[)]$', source_file.stem):
        j = -2
        new_file_name = source_file.stem[:-1]
        while new_file_name[j] != ' ':
            j -= 1
        new_file_name = Path(f'{new_file_name[:j]}{source_file.suffix}')

    else:
        new_file_name = Path(file_name)

    stem = new_file_name.stem
    suffix = source_file.suffix
    while sink_dir.joinpath(f'{stem} ({i}){suffix}').exists():
        i += 1

    new_file_name = f'{stem} ({i}){suffix}'

    # Once a unique filename is found, move the file to the sink with it
    source_file.rename(sink_dir.joinpath(new_file_name))


def to_path(string: str) -> Path:
    """
    Makes given string a Path object if str type; returns same Path object if
    already one
    """
    if not isinstance(string, Path):
        return Path(string).absolute()

    return string
