# This file is part of Monthly Organizer.
#
# Monthly Organizer is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# Monthly Organizer is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# Monthly Organizer. If not, see <https://www.gnu.org/licenses/>.

from json_class import CacheData

COMPRESSED_SUFFIXES = ['\\.zip$', '\\.tar$', '\\.lzma$', '\\.bz2$', '\\.gz$']
PROGRAM_NAME = 'monthlyOrganizer'
TIME_ACCESSED = 'Accessed'
TIME_CREATED = 'Created'
TIME_MODIFIED = 'Modified'
data = CacheData()
