if not exist ..\..\etc\requirements.txt goto noRequirements
python --version 3>NUL
if not errorlevel 0 goto noPython
if exist "%PROGRAMFILES(x86)%\Monthly Organizer" goto alreadyInstalled

if exist venv (
	rmdir /s venv
)
if exist dist (
	rmdir /s dist
)
if exist build (
	rmdir /s build
)

python -m venv venv
call venv\Scripts\activate.bat
pip install -r ..\..\etc\requirements.txt
pip install pyinstaller
pyinstaller --noconfirm --onedir --windowed --name "Monthly Organizer" --add-data "../../src/globals.py;." --add-data "../../src/json_class.py;." --add-data "../../src/linked_list.py;." --add-data "../../src/organizers.py;." --add-data "../../src/UiFiles;UiFiles/" -i ..\..\etc\MonthlyOrganizerIcon.ico  "../../src/main_gui.py"
mkdir "dist\Monthly Organizer\etc"
copy ..\..\etc\MonthlyOrganizerIcon.png "dist\Monthly Organizer\etc\MonthlyOrganizerIcon.png"
rmdir /s /q build

echo ""
echo ""
echo "Executable created. Now move 'Monthly Organizer' folder from dist folder to Program Files (x86) then pin 'Monthly Organizer.exe' to start by right-clicking it and pressing 'Pin to start'."
pause
goto:eof

:noRequirements
echo "Python requirements not available, exiting installer..."
pause
goto:eof

:noPython
echo "Python not installed or in PATH, exiting installer..."
pause
goto:eof

:alreadyInstalled
echo "Monthly Organizer already installed under Program Files (x86)..."
pause
goto:eof

