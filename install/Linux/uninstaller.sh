#!/bin/sh

if [ $(id -u) -ne 0 ]; then
	LOCAL_LIB=${HOME}/.local/lib
	APP_DIR=${XDG_DATA_HOME:-$HOME/.local/share}/applications
	LOCAL_BIN=${HOME}/.local/bin
	[ -d ${XDG_DATA_HOME}/icons ] && ICONS_DIR=${XDG_DATA_HOME}/icons || ICONS_DIR=${HOME}/.icons

	[ -d ${XDG_CACHE_HOME}/monthlyOrganizer ] && rm -rv ${XDG_CACHE_HOME}/monthlyOrganizer

	if [ -d ${XDG_CONFIG_HOME}/monthlyOrganizer ]; then
		printf "\n\nDo you wish to remove all saved profiles? (y/N)\n"
		read CONFIG_DEL_CHOICE
		LOWER_CHOICE="$(echo ${CONFIG_DEL_CHOICE} | tr "[:upper:]" "[:lower:]")"
		case ${LOWER_CHOICE} in 
			"y" | "yes")
				rm -rv ${XDG_CONFIG_HOME}/monthlyOrganizer
				;;
		esac
	fi

else
	LOCAL_LIB=/usr/local/lib
	APP_DIR=/usr/local/share/applications
	LOCAL_BIN=/usr/local/bin
	ICONS_DIR=/usr/local/share/icons

fi

[ -d ${LOCAL_LIB}/monthlyOrganizer ] && rm -rv ${LOCAL_LIB}/monthlyOrganizer
[ -f ${APP_DIR}/MonthlyOrganizer.desktop ] && rm -v ${APP_DIR}/MonthlyOrganizer.desktop
[ -f ${LOCAL_BIN}/monthlyOrganizer ] && rm -v ${LOCAL_BIN}/monthlyOrganizer
[ -f ${ICONS_DIR}/MonthlyOrganizerIcon.png ] && rm -v ${ICONS_DIR}/MonthlyOrganizerIcon.png

exit 0

