#!/bin/sh

ETC_DIR=../../etc

if [ $(id -u) -ne 0 ]; then
	ETC_SPECIFIC_DIR=${ETC_DIR}/local
	LOCAL_LIB=${HOME}/.local/lib
	APP_DIR=${XDG_DATA_HOME:-$HOME/.local/share}/applications
	LOCAL_BIN=${HOME}/.local/bin
	[ -d ${XDG_DATA_HOME}/icons ] && ICONS_DIR=${XDG_DATA_HOME}/icons || ICONS_DIR=${HOME}/.icons

else
	ETC_SPECIFIC_DIR=${ETC_DIR}/system
	LOCAL_LIB=/usr/local/lib
	APP_DIR=/usr/local/share/applications
	LOCAL_BIN=/usr/local/bin
	ICONS_DIR=/usr/local/share/icons

fi

[ ! -f ${ETC_DIR}/requirements.txt ] && printf "requirements.txt not available" && exit 1
[ ! -f ${ETC_SPECIFIC_DIR}/MonthlyOrganizer.desktop ] && printf "${ETC_SPECIFIC_DIR}/MonthlyOrganizer.desktop not available\n" && exit 1
[ ! -f ${ETC_DIR}/MonthlyOrganizerIcon.png ] && printf "${ETC_DIR}/MonthlyOrganizerIcon.png not available\n" && exit 1
[ ! -f ${ETC_SPECIFIC_DIR}/monthlyOrganizer ] && printf "${ETC_SPECIFIC_DIR}/monthlyOrganizer executable not available\n" && exit 1
[ -z "$(python3 --version | grep "^Python 3")" ] && printf "Python version 3 not available\n" && exit 1

[ ! -d "${LOCAL_LIB}/monthlyOrganizer" ] && mkdir -vp "${LOCAL_LIB}/monthlyOrganizer"
[ ! -d "${APP_DIR}" ] && mkdir -vp "${APP_DIR}"
[ ! -d "${LOCAL_BIN}" ] && mkdir -vp "${LOCAL_BIN}"
[ ! -d "${ICONS_DIR}" ] && mkdir -vp "${ICONS_DIR}"

printf "Creating virtual environment...\n"
python3 -m venv "${LOCAL_LIB}/monthlyOrganizer/venv" || (printf "Failed to create virtual environment" && exit 1)
cp -v ${ETC_DIR}/requirements.txt "${LOCAL_LIB}/monthlyOrganizer"
printf "Adding pip packages to virtual environment...\n"
. ${LOCAL_LIB}/monthlyOrganizer/venv/bin/activate
pip install -r "${LOCAL_LIB}/monthlyOrganizer/requirements.txt"
deactivate

printf "Adding .desktop file, icon, and exeutable to their respetive diretories...\n"
cp -v ${ETC_SPECIFIC_DIR}/MonthlyOrganizer.desktop "${APP_DIR}/MonthlyOrganizer.desktop"
cp -v ${ETC_SPECIFIC_DIR}/monthlyOrganizer "${LOCAL_BIN}/monthlyOrganizer"
cp -v ${ETC_DIR}/MonthlyOrganizerIcon.png "${ICONS_DIR}/MonthlyOrganizerIcon.png"
mkdir -v "${LOCAL_LIB}/monthlyOrganizer/etc"
cp -v ${ETC_DIR}/MonthlyOrganizerIcon.png "${LOCAL_LIB}/monthlyOrganizer/etc/MonthlyOrganizerIcon.png"
chmod +x "${LOCAL_BIN}/monthlyOrganizer"

cp -rv ../../src "${LOCAL_LIB}/monthlyOrganizer/src"

exit 0

