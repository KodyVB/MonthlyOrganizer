[[_TOC_]]

# Installation

## Linux

To install Monthly Organizer on Linux, first ensure you have Python (version 3.10 was tested), pip, and git installed.
Then, download the repo with the download button near the top of the page or clone this git repo with:

```sh
git clone https://www.gitlab.com/KodyVB/MonthlyOrganizer
```

Inside of the directory `install/Linux` of the cloned git repo should be an executable script called "installer.sh" - run that.
This script does the following:

1. Sets up the library, share's application, and bin directories if they're not already there
2. Creates a Python virtual environment inside of the library directory
3. Adds the .desktop file to the application directory, the shell script that runs the Python program inside the virtual environment to the bin directory, the icon to the icon directory, and the main logic from the src directory to the virtual environment 

If you run the command with root privileges, i.e. `sudo ./installer.sh`, it will be installed system-wide in directories under `/usr/local`.
If you run the command without root privileges, it will use directories under `$HOME` like `~/.local/bin`.

## Windows

### Pre-compiled Executable

To install Monthly Organizer with a pre-compiled executable, first download and extract the zip file from [this Mega.nz link](https://mega.nz/folder/P843zboJ#3SkE0wxuJkM7NbhUniKuHA).
Then place the folder into the `%PROGRAMFILES(x86)%`, which is usually located in `C:\Program Files (x86)`.
Finally, right-click on "Monthly Organizer.exe" inside of that directory and pin it to start.

### Compiling Executable Locally

To compile the executable locally, first make sure you have Python (version 3.10 was tested) as well as pip installed.
Then download this git repo with the download button near the top of the page.
Next, run the executable "installer.bat" located in `install/Windows` - this will use Python to create a virtual environment, pip to install the required packages to the virtual environment, and pyinstaller to create an executable from it.
If it was done correctly, there should be instructions to move the "Monthly Organizer" folder to "Program Files (x86)" and pin "Monthly Organizer.exe" to start by right-clicking it and selecting the option to pin to start.

# Uninstalling

## Linux

To uninstall Monthly Organizer, run the executable script "uninstaller.sh", found in the `install/Linux` directory of the git repo that the installer was located.

## Windows

To uninstall Monthly Organizer, navigate to the installed location (typically `C:\Program Files (x86)\Monthly Organizer` and right-click "Monthly Organizer.exe", and unpin it from start.
Then delete the "Monthly Organizer" directory.

